const os = require("os");
process.stdin.setEncoding("utf-8");

function getOSinfo() {
  var type = os.type();
  var release = os.release();
  var cpu = os.cpus()[0];
  var currentUser = os.userInfo();
  if (type === "Darwin") {
    type = "OSX";
  } else if (type === "Windows_NT") {
    type = "Windows";
  }
  console.log("System:", type);
  console.log("Release:", release);
  process.stdout.write(`CPU: ${cpu.model}\n`);
  process.stdout.write(`User: ${currentUser.username}\n`);
  process.stdout.write(`Homedir: ${currentUser.homedir}\n`);
}

exports.print = getOSinfo;
